from datetime import datetime
from elasticsearch import Elasticsearch
import pymysql.cursors
from flask import Flask, jsonify, request, Response, session, make_response
import json
app = Flask(__name__)



@app.route("/api/v1/payment",methods=["POST"])
def payment():
    date = datetime.now().strftime("%Y-%m-%d")
    index = "statement-" + date
    data = request.json
    first_name = data['first_name']
    email = data['email']
    action = "payment"
    amount = data['amount']
    doc = {
        'name': first_name,
        'email': email,
        'action': action,
        'amount' : amount,
        'timestamp': datetime.now(),
    }
    es = Elasticsearch([{'host': 'elasticsearch-logging.techops', 'port': 9200}])
    res = es.index(index=index, doc_type='tweet', id=1, body=doc)
    return "success"



if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8010)
