import pymysql.cursors
from flask import Flask, jsonify, request, Response, session, make_response
import json
app = Flask(__name__)


@app.route("/api/v1/test",methods=["GET"])
def test():
    return "success"

@app.route("/api/v1/insert_user",methods=["POST"])
def insert_user():

    data = request.json
    print data
    fname = data['first_name']
    lname = data['last_name']
    email = data['email']
    passwd = data['password']
    try:
        connection = pymysql.connect(host='mysql-regis.techops',
                                     user='root',
                                     password='passwd',
                                     db='db_regis',
                                     charset='utf8',
                                     port=3306,
                                     cursorclass=pymysql.cursors.DictCursor)
        with connection.cursor() as cursor:
            sql = "INSERT INTO `user` (`first_name`,`last_name`, `email`, `password`) VALUES (%s, %s, %s, %s)"
            cursor.execute(sql, (fname, lname, email, passwd))
        connection.commit()
    except:
        return "fail"
    finally:
        connection.close()
    return "success"

@app.route("/api/v1/get_users",methods=["GET"])
def get_users():

    try:
        connection = pymysql.connect(host='mysql-regis.techops',
                                     user='root',
                                     password='passwd',
                                     db='db_regis',
                                     charset='utf8',
                                     port=3306,
                                     cursorclass=pymysql.cursors.DictCursor)
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT `id`,`first_name`,`last_name` ,`email` FROM `user`"
            cursor.execute(sql)
            result = cursor.fetchall()
            return json.dumps(result)
    except:
        return "fail"
    finally:
        connection.close()



if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000)
