CREATE TABLE `user` (
	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	password varchar(100) NOT NULL,
	balance DOUBLE DEFAULT 0 
)
